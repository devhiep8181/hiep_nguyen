import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:week1_day4_7_6_mini_app/app/app_routes.dart';
import 'package:week1_day4_7_6_mini_app/views/blocs/image/image_bloc.dart';
import 'package:week1_day4_7_6_mini_app/views/blocs/preload/preload_bloc.dart';
import 'package:week1_day4_7_6_mini_app/views/video_page/video_page.dart';

void main(List<String> args) {
  runApp(
    DevicePreview(
      enabled: !kReleaseMode,
      builder: (context) => const MyApp(), // Wrap your app
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PreloadBloc(),
        ),
        BlocProvider(
          create: (context) => ImageBloc(),
        ),
      ],
      child: MaterialApp.router(
        locale: DevicePreview.locale(context),
        builder: DevicePreview.appBuilder,
        routerConfig: router,
      ),
    );
  }
}
