import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:week1_day4_7_6_mini_app/models/video_model/video_model.dart';

class VideoController {
  Future<List<VideoModel>> getVideo({required int index}) async {
    try {
      print('call get video');
      final response = await http.get(
        Uri.parse(
            'https://pixabay.com/api/videos/?key=42722516-243c8fc20447043f9e122b785&per_page=5&page=$index'),
      );
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body) as Map<String, dynamic>;
        final listVideo = <VideoModel>[];
        final listData = data['hits'];
        for (int i = 0; i < listData.length; i++) {
          listVideo.add(VideoModel.fromMap(listData[i]));
        }
        print('length: ${listVideo.length}');
        return listVideo;
      }
      return <VideoModel>[];
    } catch (e) {
      print('eeee: $e');
      return <VideoModel>[];
    }
  }
}
