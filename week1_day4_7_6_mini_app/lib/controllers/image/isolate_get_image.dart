import 'dart:developer';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:week1_day4_7_6_mini_app/controllers/image/image_controller.dart';
import 'package:week1_day4_7_6_mini_app/models/image_model/image_model.dart';
import 'package:week1_day4_7_6_mini_app/views/blocs/image/image_bloc.dart';

Future cretateIsolateImage(
    {required int index,
    required BuildContext context,
   }) async {
  ReceivePort mainReceivePort = ReceivePort(); //cổng nhận chính

  Isolate.spawn(getImagesTask, mainReceivePort.sendPort);

  SendPort isolateSendPort = await mainReceivePort.first;

  ReceivePort isolateResponseReceivePort = ReceivePort();

  isolateSendPort
      .send([index, isolateResponseReceivePort.sendPort]);

  final isolateResponse = await isolateResponseReceivePort.first;

  final listImage = isolateResponse;

  //call event update url
  BlocProvider.of<ImageBloc>(context)
      .add(UpdateImageEvent(listImage: listImage, index: index));
}

void getImagesTask(SendPort mySendPort) async {
  ReceivePort isolateReceivePort = ReceivePort();

  mySendPort.send(isolateReceivePort.sendPort);

  await for (var message in isolateReceivePort) {
    if (message is List) {
      final int index = message[0];

      final SendPort isolateResponseSendPort = message[1];

      final List<ImageModel> imageList =
          await ImageController().getImage(index: index);

     
      isolateResponseSendPort.send(imageList);
    }
  }
}
