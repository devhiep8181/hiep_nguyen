import 'dart:convert';

import 'package:week1_day4_7_6_mini_app/models/image_model/image_model.dart';
import 'package:http/http.dart' as http;

class ImageController {
  Future<List<ImageModel>> getImage({required int index}) async {
    try {
      print('call get video');
      final response = await http.get(
        Uri.parse(
            'https://pixabay.com/api/?key=42722516-243c8fc20447043f9e122b785&per_page=15&page=$index'),
      );
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body) as Map<String, dynamic>;
        final listImage = <ImageModel>[];
        final listData = data['hits'];
        for (int i = 0; i < listData.length; i++) {
          listImage.add(ImageModel.fromMap(listData[i]));
        }
        return listImage;
      }
      return <ImageModel>[];
    } catch (e) {
      print('eeee: $e');
      return <ImageModel>[];
    }
  }
}
