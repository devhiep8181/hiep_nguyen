import 'dart:convert';

import 'package:equatable/equatable.dart';

class ImageModel extends Equatable {
  final int id;
  final String? tags;
  final String? imageUrl;
  const ImageModel({
    required this.id,
    required this.tags,
    required this.imageUrl,
  });

  @override
  List<Object?> get props => [id, tags, imageUrl];

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'tags': tags,
      'imageUrl': imageUrl,
    };
  }

  factory ImageModel.fromMap(Map<String, dynamic> map) {
    return ImageModel(
      id: map['id']?.toInt() ?? 0,
      tags: map['tags'] ?? '',
      imageUrl: map['largeImageURL'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory ImageModel.fromJson(String source) => ImageModel.fromMap(json.decode(source));
}
