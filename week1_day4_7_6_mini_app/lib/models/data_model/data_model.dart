import 'dart:convert';

import 'package:equatable/equatable.dart';

import '../video_model/video_model.dart';

class DataModel extends Equatable {
  final int? total;
  final int? totalHist;
  final List<VideoModel> hits;
  const DataModel({
    required this.total,
    required this.totalHist,
    required this.hits,
  });

  Map<String, dynamic> toMap() {
    return {
      'total': total,
      'totalHist': totalHist,
      'hits': hits.map((x) => x.toMap()).toList(),
    };
  }

  factory DataModel.fromMap(Map<String, dynamic> map) {
    return DataModel(
      total: map['total']?.toInt(),
      totalHist: map['totalHist']?.toInt(),
      hits:
          List<VideoModel>.from(map['hits']?.map((x) => VideoModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory DataModel.fromJson(String source) =>
      DataModel.fromMap(json.decode(source));

  @override
  List<Object?> get props => [total, totalHist, hits];
}
