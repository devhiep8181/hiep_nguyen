import 'dart:convert';

import 'package:equatable/equatable.dart';

class TinyVideoModel extends Equatable {
    final String? url;
    final int? width;
    final int? height;
    final int? size;
    final String? thumbnail;

    const TinyVideoModel({
        required this.url,
        required this.width,
        required this.height,
        required this.size,
        required this.thumbnail,
    });

  @override
  List<Object?> get props => [url, width, height, size, thumbnail];

  

  Map<String, dynamic> toMap() {
    return {
      'url': url,
      'width': width,
      'height': height,
      'size': size,
      'thumbnail': thumbnail,
    };
  }

  factory TinyVideoModel.fromMap(Map<String, dynamic> map) {
    return TinyVideoModel(
      url: map['url'],
      width: map['width']?.toInt(),
      height: map['height']?.toInt(),
      size: map['size']?.toInt(),
      thumbnail: map['thumbnail'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TinyVideoModel.fromJson(String source) => TinyVideoModel.fromMap(json.decode(source));
}
