import 'dart:convert';

import '../tiny_video_model/large_video_model.dart';


class VideoModel {
    final int? id;
    final String? pageUrl;
    final String? type;
    final String? tags;
    final int? duration;
    final TinyVideoModel videos;
    final int? views;
    final int? downloads;
    final int? likes;
    final int? comments;
    final int? userId;
    final String? user;
    final String? userImageUrl;

    VideoModel({
        required this.id,
        required this.pageUrl,
        required this.type,
        required this.tags,
        required this.duration,
        required this.videos,
        required this.views,
        required this.downloads,
        required this.likes,
        required this.comments,
        required this.userId,
        required this.user,
        required this.userImageUrl,
    });


  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'pageUrl': pageUrl,
      'type': type,
      'tags': tags,
      'duration': duration,
      'videos': videos.toMap(),
      'views': views,
      'downloads': downloads,
      'likes': likes,
      'comments': comments,
      'userId': userId,
      'user': user,
      'userImageUrl': userImageUrl,
    };
  }

  factory VideoModel.fromMap(Map<String, dynamic> map) {
    return VideoModel(
      id: map['id']?.toInt(),
      pageUrl: map['pageUrl'],
      type: map['type'],
      tags: map['tags'],
      duration: map['duration']?.toInt(),
      videos: TinyVideoModel.fromMap(map['videos']['tiny']),
      views: map['views']?.toInt(),
      downloads: map['downloads']?.toInt(),
      likes: map['likes']?.toInt(),
      comments: map['comments']?.toInt(),
      userId: map['userId']?.toInt(),
      user: map['user'],
      userImageUrl: map['userImageUrl'],
    );
  }

  String toJson() => json.encode(toMap());

  factory VideoModel.fromJson(String source) => VideoModel.fromMap(json.decode(source));
}
