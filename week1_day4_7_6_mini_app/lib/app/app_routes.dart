import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:week1_day4_7_6_mini_app/app/app_name_routes.dart';
import 'package:week1_day4_7_6_mini_app/views/home_page/home_screen.dart';
import 'package:week1_day4_7_6_mini_app/views/image_page/image_screen.dart';
import 'package:week1_day4_7_6_mini_app/views/splash_page/splash_screen.dart';
import 'package:week1_day4_7_6_mini_app/views/video_page/video_page.dart';

final GoRouter router = GoRouter(
  initialLocation: '/$splashRoute',
  routes: [
  ShellRoute(
      builder: (context, state, child) {
        return SafeArea(child: child);
      },
      routes: [
        GoRoute(
            name: splashRoute,
            path: '/$splashRoute',
            pageBuilder: (context, state) {
              return const MaterialPage(child: SplashScreen());
            },
            routes: [
              GoRoute(
                  path: homeRoute,
                  name: homeRoute,
                  pageBuilder: (context, state) {
                    return const MaterialPage(child: HomeScreen());
                  },
                  routes: [
                    GoRoute(
                      path: videoRoute,
                      name: videoRoute,
                      pageBuilder: (context, state) {
                        return const MaterialPage(child: VideoPage());
                      },
                    ),
                        GoRoute(
                      path: imageRoute,
                      name: imageRoute,
                      pageBuilder: (context, state) {
                        return const MaterialPage(child: ImageScreen());
                      },
                    ),
                  ])
            ])
      ])
]);
