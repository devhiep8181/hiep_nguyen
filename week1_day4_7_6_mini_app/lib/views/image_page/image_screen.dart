import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:week1_day4_7_6_mini_app/controllers/image/isolate_get_image.dart';
import 'package:week1_day4_7_6_mini_app/views/blocs/image/image_bloc.dart';
import 'package:week1_day4_7_6_mini_app/widgets/custom_image_view.dart';

class ImageScreen extends StatefulWidget {
  const ImageScreen({super.key});

  @override
  State<ImageScreen> createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    context.read<ImageBloc>().add(const GetImageEvent(index: 1));
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.offset >= 10) {
      int index = BlocProvider.of<ImageBloc>(context).state.index;
      index++;
      log('INDEX:======> $index');
      cretateIsolateImage(index: index, context: context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('View Image'),
        backgroundColor: Colors.amber,
      ),
      body: BlocBuilder<ImageBloc, ImageState>(
        builder: (context, state) {
          if (state.imageStatus.isLoaded) {
            return Center(
                child: MasonryGridView.count(
              controller: _scrollController,
              crossAxisCount: 2,
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
              itemCount: state.listImage.length,
              itemBuilder: (context, index) {
                return CustomImageView(
                  imagePath: state.listImage[index].imageUrl,
                  fit: BoxFit.fitWidth,
                );
              },
            ));
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
