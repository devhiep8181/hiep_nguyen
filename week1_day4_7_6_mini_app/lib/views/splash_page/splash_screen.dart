import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:week1_day4_7_6_mini_app/app/app_name_routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2), () {
      context.goNamed(homeRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () => context.goNamed(homeRoute),
        child: const Center(
          child: Text(
            'Welcome to SUPER IDO ^^',
            style: TextStyle(fontSize: 30, color: Colors.amber),
          ),
        ),
      ),
    );
  }
}
