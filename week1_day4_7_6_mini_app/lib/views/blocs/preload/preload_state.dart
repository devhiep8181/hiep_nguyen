part of 'preload_bloc.dart';

enum PreloadStatus { init, loading, loaded, error }

extension PreloadStatusX on PreloadStatus {
  bool get isLoaded => [PreloadStatus.loaded].contains(this);
}

class PreloadState extends Equatable {
  const PreloadState(
      {required this.urls,
      required this.controller,
      required this.focusedIndex,
      required this.isLoading,
      required this.reloadCounter,
      required this.preloadStatus,
      });

  final List<String> urls;
  final Map<int, VideoPlayerController> controller;
  final int focusedIndex;
  final int reloadCounter;
  final bool isLoading;
  final PreloadStatus preloadStatus;

  @override
  List<Object> get props =>
      [urls, controller, focusedIndex, isLoading, reloadCounter, preloadStatus];

  PreloadState copyWith({
    List<String>? urls,
    Map<int, VideoPlayerController>? controller,
    int? focusedIndex,
    int? reloadCounter,
    bool? isLoading,
    PreloadStatus? preloadStatus,
  }) {
    return PreloadState(
      urls: urls ?? this.urls,
      controller: controller ?? this.controller,
      focusedIndex: focusedIndex ?? this.focusedIndex,
      reloadCounter: reloadCounter ?? this.reloadCounter,
      isLoading: isLoading ?? this.isLoading,
      preloadStatus: preloadStatus ?? this.preloadStatus,
    );
  }
}
