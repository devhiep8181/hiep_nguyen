import 'dart:async';
import 'dart:developer';
import 'dart:isolate';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';
import 'package:week1_day4_7_6_mini_app/controllers/video/video_controler.dart';
import 'package:week1_day4_7_6_mini_app/core/utils/constant.dart';
import 'package:week1_day4_7_6_mini_app/models/video_model/video_model.dart';

part 'preload_event.dart';
part 'preload_state.dart';

class PreloadBloc extends Bloc<PreloadEvent, PreloadState> {
  PreloadBloc()
      : super(const PreloadState(
            urls: [],
            controller: {},
            focusedIndex: 0,
            isLoading: false,
            reloadCounter: 1,
            preloadStatus: PreloadStatus.init)) {
    on<GetVideosFromApi>(_onGetVideosFromApi);
    on<VideoIndexChanged>(_onVideoIndexChanged);
    on<UpdateUrlEvent>(_onUpdateUrlEvent);
    on<EmptyVideoEvent>(_onEmptyVideoEvent);
  }

  FutureOr<void> _onGetVideosFromApi(
      GetVideosFromApi event, Emitter<PreloadState> emit) async {
    final List<VideoModel> videoModel =
        await VideoController().getVideo(index: state.reloadCounter);
    // List<String> listUrl = [];
    // for (int i = 0; i < videoModel.length; i++) {
    //   listUrl.add(videoModel[i].videos.url ?? '');
    // }
    List<String> listUrl =
        videoModel.map((videoItem) => videoItem.videos.url ?? '').toList();
    print('lenght listview: ${listUrl.length}');

    emit(state.copyWith(urls: listUrl));

    await _initControllerAtIndex(0, emit);
    _playControllerVideo(0);
    await _initControllerAtIndex(1, emit);

    emit(state.copyWith(preloadStatus: PreloadStatus.loaded, reloadCounter: 2));
  }

  Future<void> _initControllerAtIndex(
      int index, Emitter<PreloadState> emit) async {
    final listVideo = List<String>.from(state.urls);

    var mapController = Map<int, VideoPlayerController>.from(state.controller);

    if (listVideo.length > index && index >= 0) {
      final VideoPlayerController _controller =
          VideoPlayerController.networkUrl(Uri.parse(listVideo[index]));

      mapController[index] = _controller;
      emit(state.copyWith(controller: mapController));

      await _controller.initialize();

      log('🚀🚀🚀 INITIALIZED $index');
    }
  }

  void _playControllerVideo(int index) {
    if (state.urls.length > index && index >= 0) {
      final VideoPlayerController _controller = state.controller[index]!;

      _controller.play();

      log('🚀🚀🚀 PLAYING $index');
    }
  }

  FutureOr<void> _onVideoIndexChanged(
      VideoIndexChanged event, Emitter<PreloadState> emit) {
    final bool shouldFetch = (event.index + kPreloadLimit) % kNextLimit == 0 &&
        state.urls.length == event.index + kPreloadLimit;

    var reloadCounter = state.reloadCounter;
    if (shouldFetch) {
      cretateIsolate(
          index: event.index,
          context: event.context,
          reloadCounter: reloadCounter);
      reloadCounter++;
    }

    if (event.index > state.focusedIndex) {
      _playNext(event.index, emit);
    } else {
      _playPrevious(event.index, emit);
    }

    emit(state.copyWith(
        focusedIndex: event.index, reloadCounter: reloadCounter));
  }

  void _playNext(int index, Emitter<PreloadState> emit) {
    /// Stop [index - 1] controller
    _stopControllerAtIndex(index - 1, emit);

    /// Dispose [index - 2] controller
    _disposeControllerAtIndex(index - 2, emit);

    /// Play current video (already initialized)
    _playControllerVideo(index);

    /// Initialize [index + 1] controller
    _initControllerAtIndex(index + 1, emit);
  }

  void _playPrevious(int index, Emitter<PreloadState> emit) {
    /// Stop [index + 1] controller
    _stopControllerAtIndex(index + 1, emit);

    /// Dispose [index + 2] controller
    _disposeControllerAtIndex(index + 2, emit);

    /// Play current video (already initialized)
    _playControllerVideo(index);

    /// Initialize [index - 1] controller
    _initControllerAtIndex(index - 1, emit);
  }

  void _stopControllerAtIndex(int index, Emitter<PreloadState> emit) {
    if (state.urls.length > index && index >= 0) {
      final VideoPlayerController _controller = state.controller[index]!;

      _controller.pause();

      _controller.seekTo(const Duration());

      log('🚀🚀🚀 STOPPED $index');
    }
  }

  void _disposeControllerAtIndex(int index, Emitter<PreloadState> emit) {
    if (state.urls.length > index && index >= 0) {
      final VideoPlayerController _controller = state.controller[index]!;

      _controller.dispose();

      var mapController =
          Map<int, VideoPlayerController>.from(state.controller);

      mapController.remove(index);
      log('length mapController: ${mapController.length}');
      emit(state.copyWith(controller: mapController));

      log('🚀🚀🚀 STOPPED $index');
    }
  }

  FutureOr<void> _onUpdateUrlEvent(
      UpdateUrlEvent event, Emitter<PreloadState> emit) {
    final listUrl = List<String>.from(state.urls)..addAll(event.urls);

    _initControllerAtIndex(state.focusedIndex + 1, emit);
    emit(state.copyWith(urls: listUrl));
  }

  FutureOr<void> _onEmptyVideoEvent(
      EmptyVideoEvent event, Emitter<PreloadState> emit) {
    emit(state.copyWith(preloadStatus: PreloadStatus.init));
  }
}

Future cretateIsolate(
    {required int index,
    required BuildContext context,
    required int reloadCounter}) async {
  ReceivePort mainReceivePort = ReceivePort(); //cổng nhận chính

  Isolate.spawn(getVideosTask, mainReceivePort.sendPort);

  SendPort isolateSendPort = await mainReceivePort.first;

  ReceivePort isolateResponseReceivePort = ReceivePort();

  isolateSendPort
      .send([index, isolateResponseReceivePort.sendPort, reloadCounter]);

  final isolateResponse = await isolateResponseReceivePort.first;

  final _urls = isolateResponse;

  //call event update url
  BlocProvider.of<PreloadBloc>(context, listen: false)
      .add(UpdateUrlEvent(urls: _urls));
}

void getVideosTask(SendPort mySendPort) async {
  ReceivePort isolateReceivePort = ReceivePort();

  mySendPort.send(isolateReceivePort.sendPort);

  await for (var message in isolateReceivePort) {
    if (message is List) {
      final int index = message[0];

      final SendPort isolateResponseSendPort = message[1];

      final int reloadCounter = message[2];

      log('RELOAD COUNTER ====>: $reloadCounter');

      final List<VideoModel> videoModel =
          await VideoController().getVideo(index: reloadCounter);

      List<String> listUrl =
          videoModel.map((videoItem) => videoItem.videos.url ?? '').toList();

      isolateResponseSendPort.send(listUrl);
    }
  }
}
