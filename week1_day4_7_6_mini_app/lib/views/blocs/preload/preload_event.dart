part of 'preload_bloc.dart';

abstract class PreloadEvent extends Equatable {
  const PreloadEvent();

  @override
  List<Object> get props => [];
}

class GetVideosFromApi extends PreloadEvent {}

class VideoIndexChanged extends PreloadEvent {
  final int index;
  final BuildContext context;
  const VideoIndexChanged({
    required this.index,
    required this.context
  });

  @override
  List<Object> get props => [index, context];
}

class UpdateUrlEvent extends PreloadEvent {
  final List<String> urls;
  const UpdateUrlEvent({
    required this.urls,
  });

  @override
  List<Object> get props => [urls];
}


class EmptyVideoEvent extends PreloadEvent{}