part of 'image_bloc.dart';

abstract class ImageEvent extends Equatable {
  const ImageEvent();

  @override
  List<Object> get props => [];
}

class GetImageEvent extends ImageEvent {
  final int index;
  const GetImageEvent({
    required this.index,
  });

  @override
  List<Object> get props => [index];
}

class UpdateImageEvent extends ImageEvent {
  final List<ImageModel> listImage;
  final int index;
  const UpdateImageEvent({
    required this.listImage,
    required this.index,
  });
  @override
  List<Object> get props => [listImage, index];
}
