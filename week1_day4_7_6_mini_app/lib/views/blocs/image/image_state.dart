part of 'image_bloc.dart';

enum ImageStatus { init, loading, loaded, error }

extension ImageStatusX on ImageStatus {
  bool get isLoaded => [ImageStatus.loaded].contains(this);
}

class ImageState extends Equatable {
  const ImageState({
    required this.listImage,
    required this.imageStatus,
    required this.index,
  });

  final List<ImageModel> listImage;
  final ImageStatus imageStatus;
  final int index;

  @override
  List<Object> get props => [listImage, imageStatus, index];

  ImageState copyWith({
    List<ImageModel>? listImage,
    ImageStatus? imageStatus,
    int? index,
  }) {
    return ImageState(
      listImage: listImage ?? this.listImage,
      imageStatus: imageStatus ?? this.imageStatus,
      index: index ?? this.index,
    );
  }
}
