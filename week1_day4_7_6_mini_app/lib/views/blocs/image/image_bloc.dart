import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:week1_day4_7_6_mini_app/controllers/image/image_controller.dart';
import 'package:week1_day4_7_6_mini_app/models/image_model/image_model.dart';
import 'package:week1_day4_7_6_mini_app/models/video_model/video_model.dart';

part 'image_event.dart';
part 'image_state.dart';

class ImageBloc extends Bloc<ImageEvent, ImageState> {
  ImageBloc()
      : super(const ImageState(listImage: [], imageStatus: ImageStatus.init, index: 1)) {
    on<GetImageEvent>(_onGetImageEvent);
    on<UpdateImageEvent>(_onUpdateImageEvent);
  }

  FutureOr<void> _onGetImageEvent(
      GetImageEvent event, Emitter<ImageState> emit) async {
    try {
      emit(state.copyWith(imageStatus: ImageStatus.loading));
      final listImage = await ImageController().getImage(index: event.index);
      listImage.forEach(
        (element) => log('IMAGE: $element'),
      );
      emit(state.copyWith(
          listImage: listImage, imageStatus: ImageStatus.loaded));
    } catch (e) {
      emit(state.copyWith(imageStatus: ImageStatus.error));
    }
  }

  FutureOr<void> _onUpdateImageEvent(
      UpdateImageEvent event, Emitter<ImageState> emit) async {
    final listImage = List<ImageModel>.from(state.listImage);
    
    listImage.addAll(event.listImage);

    emit(state.copyWith(listImage: listImage, index: event.index));
  }
}
