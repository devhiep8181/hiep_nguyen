import 'dart:developer';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

import '../blocs/preload/preload_bloc.dart';

final List<String> items = List<String>.generate(10, (index) => 'Item $index');

class VideoPage extends StatefulWidget {
  const VideoPage({super.key});

  @override
  State<VideoPage> createState() => _VideoPageState();
}

class _VideoPageState extends State<VideoPage> {
  final PageController controller = PageController();
  @override
  void initState() {
    super.initState();
    context.read<PreloadBloc>().add(GetVideosFromApi());
  }

  @override
  Widget build(BuildContext context) {
    //TODO: LỖI KHI ĐANG Ở MÀN NÀY CHUYỂN ĐẾN MÀN KHÁC XONG CHUYỂN LẠI MÀN VIDEOPAGE TRẮNG XOÁ
    return SafeArea(child: Scaffold(
      body: BlocBuilder<PreloadBloc, PreloadState>(
        builder: (context, state) {
          if (state.preloadStatus.isLoaded) {
            return PageView.builder(
              scrollDirection: Axis.vertical,
              onPageChanged: (int index) {
                log('INDEX VIDEO: =======> $index');
                context
                    .read<PreloadBloc>()
                    .add(VideoIndexChanged(index: index, context: context));
              },
              itemBuilder: (context, index) {
                return state.focusedIndex == index
                    ? VideoWidget(
                        isLoading: true, controller: state.controller[index]!)
                    : const SizedBox();
              },
              itemCount: state.urls.length,
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    ));
  }
}

/// Custom Feed Widget consisting video
class VideoWidget extends StatefulWidget {
  const VideoWidget({
    super.key,
    required this.isLoading,
    required this.controller,
  });

  final bool isLoading;
  final VideoPlayerController controller;

  @override
  State<VideoWidget> createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FlickVideoPlayer(
          flickManager: FlickManager(videoPlayerController: widget.controller),
        ),
      ],
    );
  }
}
