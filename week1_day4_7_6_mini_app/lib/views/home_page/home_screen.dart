import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:week1_day4_7_6_mini_app/app/app_name_routes.dart';
import 'package:week1_day4_7_6_mini_app/views/blocs/image/image_bloc.dart';
import 'package:week1_day4_7_6_mini_app/views/blocs/preload/preload_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  context.goNamed(videoRoute);
                },
                child: const Text('Watch Video')),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  context.goNamed(imageRoute);
                },
                child: const Text('Image'))
          ],
        ),
      ),
    );
  }
}
