import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day2_11_6_testcase/counter.dart';

void main() {
  group('Test start, increment, decrement', () {
    test('value should start at 0 ', () {
      expect(Counter().value, 0);
    });

    test('value should be increment', () {
      final counter = Counter();
      counter.increment();

      expect(counter.value, 1);
    });

    test('value should be decrement', () {
      final counter = Counter();
      counter.decrement();
      expect(counter.value, -1);
    });
  });
}
