import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day2_11_6_testcase/example/sort_arrary.dart';

void main() {
  group('sắp xếp dãy số', () {
    test('sắp xếp tăng dần', () {
      List<int> a = [5, 4, 3, 2, 6, 8];

      //excepted
      List<int> exceptedList = [2, 3, 4, 5, 6, 8];

      //act
      final result = sortArray(a);

      expect(result, exceptedList);
    });

    test('sap xep day so', () => null);
  });
}
