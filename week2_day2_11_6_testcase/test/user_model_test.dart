import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day2_11_6_testcase/models/user_model.dart';

void main() {
  group('kiem tra user model', () {
    test('test khởi tạo và thuộc tính', () {
      final user1 = UserModel(id: 1, nameUser: 'hihi', email: 'hihi@gmail.com');

      expect(user1.id, 1);
      expect(user1.nameUser, 'hihi');
      expect(user1.email, 'hihi@gmail.com');
    });

    test('so sánh 2 đối tượng', () {
      const user1 = UserModel(id: 1, nameUser: 'hihi', email: 'hihi@gmail.com');
      const user2 = UserModel(id: 1, nameUser: 'hihi', email: 'hihi@gmail.com');

      expect(user1, user2);
    });
  });
}
