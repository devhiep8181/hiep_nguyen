//TODO: 1: Viết test cho trường hợp nhập đúng mật khẩu
//TODO: 2: Viết test cho trường hợp nhập sai mật khẩu

import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:week2_day2_11_6_testcase/unit_test_method/login_view_model.dart';

class MockSharePreferences extends Mock implements SharedPreferences {}

void main() {
  group('test login', () {
    test('login should return false when the password are incorrect', () {
      final mockSharePreferences = MockSharePreferences();
      final loginViewModel =
          LoginViewModel(sharedPreferences: mockSharePreferences);
      String email = 'hihihi@gmail.com';
      String password = 'otoku';

      //Stubing: mô phỏng kết quả trả về của phương thức getString trong class LoginView
      when(() => mockSharePreferences.getString(email)).thenReturn('123456');
      //act
      final login = loginViewModel.login(email, password);

      //except
      expect(login, false);
    });

    test('login should return true when the password are correct', () {
      //create mock object
      final mockSharePreferences = MockSharePreferences();
      final loginViewModel =
          LoginViewModel(sharedPreferences: mockSharePreferences);

      String email = 'hitachi@gmail.com';
      String password = '123456';

      //stubbing
      when(() => mockSharePreferences.getString(email)).thenReturn('123456');

      //act
      final result = loginViewModel.login(email, password);

      //except
      expect(result, true);
    });
  });

  group('test logout', () {
    test('logout should return true when the clear method returns true',
        () async {
      //create mock object
      final mockSharePreferences = MockSharePreferences();
      final loginViewModel =
          LoginViewModel(sharedPreferences: mockSharePreferences);

      //stubbing
      when(() => mockSharePreferences.clear())
          .thenAnswer((invocation) => Future.value(true));

      //act
      final call = await loginViewModel.logout();

      //expect
      expect(call, true);
    });

    test('logout should throw an exception when the clear method returns false',
        () async {
      //create mock object
      final mockSharePreferences = MockSharePreferences();
      final loginViewModel =
          LoginViewModel(sharedPreferences: mockSharePreferences);

      //stubbing
      when(() => mockSharePreferences.clear())
          .thenAnswer((invocation) => Future.value(false));

      //act
      final call = await loginViewModel.logout();

      //expect
      expect(call, throwsFlutterError);
    });

    test(
        'logout should throw an exception when the clear method throws an exception',
        () async {
      //create mock object
      final mockSharePreferences = MockSharePreferences();
      final loginViewModel =
          LoginViewModel(sharedPreferences: mockSharePreferences);

      //stubbing
      when(() => mockSharePreferences.clear())
          .thenThrow(Exception('Logout failed'));

      //act
      //act
      final Future<bool> Function() call = loginViewModel.logout;

      //expect

      
      expect(
          call,
          throwsA(isA<FlutterError>()
              .having((p0) => p0.message, 'error message', 'Logout failed')));
    });
  });

  
}
