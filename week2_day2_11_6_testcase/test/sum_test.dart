import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day2_11_6_testcase/example/sum.dart';

void main() {
  group('test sum number', () {
    test('cong 2 so nguyen duong', () {
      int a = 5;
      int b = 3;
      int expectedSum = 8;

      //act
      int actualSum = sumNumber(a, b);

      expect(actualSum, expectedSum);
    });

    test('cong 1 so nguyen duong va 1 so nguyen am', () {
      int a = 5;
      int b = -2;

      int expectedSum = 3;

      //act
      int actualSum = sumNumber(a, b);

      expect(actualSum, expectedSum);
    });

    test('cong 2 so nguyen am', () {
      int a = -5;
      int b = -6;

      int expectedSum = -11;

      //act
      int actualSum = sumNumber(a, b);

      expect(actualSum, expectedSum);
    });


  });
}
