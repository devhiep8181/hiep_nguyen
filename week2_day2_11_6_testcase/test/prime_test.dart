import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day2_11_6_testcase/example/prime.dart';

void main() {
  group('test so nguyen to', () {
    test('so nguyen to', () {
      int n = 3;
      //act
      final result = checkPrime(n);

      expect(result, true);
    });
    test('khong phai so nguyen to', () {
      int n = 10;
      final reslut = checkPrime(n);
      expect(reslut, false);
    });
  });
}
