import 'package:equatable/equatable.dart';

class UserModel extends Equatable{
  final int id;
  final String nameUser;
  final String email;
  const UserModel({
    required this.id,
    required this.nameUser,
    required this.email,
  });

  @override
  List<Object?> get props => [id, nameUser, email];
}
