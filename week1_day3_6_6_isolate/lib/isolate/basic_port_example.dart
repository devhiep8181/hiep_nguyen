import 'dart:async';
import 'dart:convert';
import 'dart:isolate';

class WorkerIsolate {
  late SendPort _sendPort; //cổng gửi của main isolate
  final Completer<void> _isolateReady = Completer.sync();

  Future<void> spawn() async {
    //thêm các chức năng để tạo một isolate worker
    final reviceMainIsolate =
        ReceivePort(); //tạo ra cổng nhận cho phép main isolate nhận thông báo từ worker
    reviceMainIsolate.listen(
        _handleResponsesFromIsolate); //lắng nghe các phản hồi từ cổng gửi của worker isolate
    await Isolate.spawn(_startRemoteIsolate,
        reviceMainIsolate.sendPort); //tạo ra worker isolate
  }

  void _handleResponsesFromIsolate(dynamic message) {
    //cổng gửi đi từ work isolate
    // xử lý phản hồi được gửi từ Worker isolate
    if (message is SendPort) {
      _sendPort = message;
      _isolateReady.complete();
    } else if (message is Map<String, dynamic>) {
      print('message siuuuuu');
    }
  }

  static void _startRemoteIsolate(SendPort port) {
    //code sẽ thực thi trong worker isolate
    final receiveWorkerIsolate = ReceivePort();
    port.send(receiveWorkerIsolate.sendPort);

    receiveWorkerIsolate.listen((message) async {
      if (message is String) {
        final transformed = jsonDecode(message);
        port.send(transformed);
      }
    });
  }

  Future<void> parseJson(String message) async {
    //cổng nhận từ main isolate
    //gửi tin nhắn từ main isolate đến worker isolate
    await _isolateReady.future; //đảm bảo rằng không có bất kỳ thứ gì được gửi đi khi worker isolate chưa được tạo ra
    _sendPort.send(message);
  }
}
