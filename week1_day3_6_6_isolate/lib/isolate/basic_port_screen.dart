
import 'package:flutter/material.dart';
import 'package:week1_day3_6_6_isolate/isolate/basic_port_example.dart';

class BasicPortScreen extends StatelessWidget {
  const BasicPortScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
            onPressed: () async {
              final worker = WorkerIsolate();
              await worker.spawn(); //tạo
              await worker.parseJson('{"key" : "value"}');
            },
            child: const Text("Call")),
      ),
    );
  }
}
