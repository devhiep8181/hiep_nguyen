import 'dart:isolate';

import 'package:flutter/material.dart';

class IsolateScreen extends StatefulWidget {
  const IsolateScreen({super.key});

  @override
  State<IsolateScreen> createState() => _IsolateScreenState();
}

class _IsolateScreenState extends State<IsolateScreen> {
  bool _loading = false;
  String text = "null";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () async {
                  setState(() {
                    _loading = !_loading;
                   //text = heavyTask1().toString();
                  });

                  //Khởi tạo isolate worker
                  final revicePort = ReceivePort();
                  // final isolate =
                  //     await Isolate.spawn(heavyTask, revicePort.sendPort);
                  final isolate = await Isolate.spawn((message) {
                    var counting = 0;
                    for (var i = 0; i <= 10000000000; i++) {
                      counting = i;
                    }
                    message.send('result: $counting!');
                  }, revicePort.sendPort);
                  revicePort.listen((message) {
                    setState(() {
                      text = message as String;
                      _loading = !_loading;
                    });
                    print('message in revice port: $message');
                    revicePort.close();
                    isolate.kill();
                  });
                },
                child: const Text("Test Isolate")),
            _loading ? const CircularProgressIndicator() : Text(text),
          ],
        ),
      ),
    );
  }
}

//b1: viết hàm isolate
void heavyTask(SendPort sendPort) {
  var counting = 0;
  for (var i = 0; i <= 10000000000; i++) {
    counting = i;
  }
  sendPort.send('result: $counting!');
}

// int heavyTask1() {
//   var counting = 0;
//   for (var i = 0; i <= 10000000000; i++) {
//     counting = i;
//   }
//   return counting;
// }
