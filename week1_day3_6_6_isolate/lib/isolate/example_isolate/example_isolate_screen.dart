
import 'package:flutter/material.dart';
import 'package:week1_day3_6_6_isolate/isolate/example_isolate/example_isolate.dart';

class ExampleIsolateScreen extends StatelessWidget {
  const ExampleIsolateScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
            onPressed: () async {
              final worker = ExampleIsolate();
              await worker.spawn(); //tạo
              await worker.sendDataFromMainIsolateToWorkerIsolate(1111111222);
            },
            child: const Text("Call")),
      ),
    );
  }
}
