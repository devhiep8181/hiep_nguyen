import 'dart:async';
import 'dart:developer';
import 'dart:isolate';

class ExampleIsolate {
  late SendPort _sendPort;
  final Completer<void> _isolateReady = Completer.sync();
  Future<void> spawn() async {
    //create worker isolate
    final reviceMainIsolate = ReceivePort();
    reviceMainIsolate.listen(_handleResponseFromIsolate);

    await Isolate.spawn(_startRemoteIsolate, reviceMainIsolate.sendPort);
  }

  void _handleResponseFromIsolate(dynamic message) {
    //cổng gửi dữ liệu từ worker isolate
    if (message is SendPort) {
      _sendPort = message;
      _isolateReady.complete();
    } else if (message is String) {
      log('message is: $message');
    }
  }

  static void _startRemoteIsolate(SendPort port) {
    //code thực thi trong isolate
    final recevieWorkerIsolate = ReceivePort();
    port.send(recevieWorkerIsolate.sendPort);

    recevieWorkerIsolate.listen((message) async {
      if (message is int) {
        final dataTransfer = message.toString();
        port.send(dataTransfer);
      }
    });
  }

  Future<void> sendDataFromMainIsolateToWorkerIsolate(int value) async {
    await _isolateReady.future;
    _sendPort.send(value);
  }
}
