import 'package:flutter/material.dart';
import 'package:week1_day3_6_6_isolate/isolate/basic_port_screen.dart';
import 'package:week1_day3_6_6_isolate/isolate/example_isolate/example_isolate.dart';
import 'package:week1_day3_6_6_isolate/isolate/example_isolate/example_isolate_screen.dart';

void main(List<String> args) {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: true,
      home: ExampleIsolateScreen(),
    );
  }
}


