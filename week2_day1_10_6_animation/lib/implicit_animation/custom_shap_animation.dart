import 'package:flutter/material.dart';

const _duration = Duration(milliseconds: 500);

class CustomShapAnimation extends StatefulWidget {
  const CustomShapAnimation({super.key});

  @override
  State<CustomShapAnimation> createState() => _CustomShapAnimationState();
}

class _CustomShapAnimationState extends State<CustomShapAnimation> {
  double _width = 200;
  double _opacity = 1;
  bool first = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                width: 200,
                child: ElevatedButton(
                    onPressed: () {}, child: const Text('Change'))),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  first = !first;
                });
              },
              child: AnimatedCrossFade(
                  firstChild: Container(
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(20)),
                      child: const Center(child: Text('Change Icon'))),
                  secondChild: const Icon(
                    Icons.light,
                    color: Colors.amber,
                  ),
                  crossFadeState: first
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  duration: const Duration(milliseconds: 500)),
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  if (_width == 200) {
                    _width = 50;
                    _opacity = 0;
                  } else {
                    _width = 200;
                    _opacity = 1;
                  }
                  // =>
                  //_width = (_width == 200) ? 50 : 200;
                });
              },
              child: AnimatedContainer(
                duration: _duration,
                width: _width,
                curve: Curves.easeInOut,
                child: _width == 200
                    ? Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(20)),
                        child: const Center(child: Text('Change Icon')))
                    : const Icon(
                        Icons.light,
                        color: Colors.amber,
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
