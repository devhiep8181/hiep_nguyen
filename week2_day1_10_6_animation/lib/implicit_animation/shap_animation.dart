import 'dart:math';

import 'package:flutter/material.dart';

///Using AnimatedContainer

const _duration = Duration(microseconds: 400);
double randomBorderRadius() {
  return Random().nextDouble() * 64;
}

double randomMargin() {
  return Random().nextDouble() * 64;
}

Color randomColor() {
  return Color(0xffffffff & Random().nextInt(0xffffffff));
}

class ShapAnimation extends StatefulWidget {
  const ShapAnimation({super.key});

  @override
  State<ShapAnimation> createState() => _ShapAnimationState();
}

class _ShapAnimationState extends State<ShapAnimation> {
  late Color color;
  late double borderRadius;
  late double margin;

  @override
  void initState() {
    super.initState();
    color = randomColor();
    borderRadius = randomBorderRadius();
    margin = randomMargin();
  }

  void change() {
    setState(() {
      color = randomColor();
      borderRadius = randomBorderRadius();
      margin = randomMargin();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              width: 128,
              height: 128,
              child: AnimatedContainer(
                  margin: EdgeInsets.all(margin),
                  decoration: BoxDecoration(
                      color: color,
                      borderRadius: BorderRadius.circular(borderRadius)),
                  duration: _duration),
            ),
            ElevatedButton(
                onPressed: () {
                  change();
                },
                child: const Text('Change')),
          ],
        ),
      ),
    );
  }
}
