import 'dart:async';

import 'package:flutter/material.dart';

const owlUrl =
    'https://raw.githubusercontent.com/flutter/website/main/src/content/assets/images/docs/owl.jpg';

class FadeInAnimation extends StatefulWidget {
  const FadeInAnimation({super.key});

  @override
  State<FadeInAnimation> createState() => _FadeInAnimationState();
}

class _FadeInAnimationState extends State<FadeInAnimation> {
  double _opacity = 0;

  @override
  void initState() {
    super.initState();
    
    Timer(const Duration(seconds: 2), () {
      setState(() {
        _opacity = 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          AnimatedOpacity(
            opacity: _opacity,
            duration: const Duration(seconds: 3),
            child: Image.network(owlUrl),
          ),
          TextButton(
              onPressed: () {
                setState(() {
                  _opacity = 1;
                });
              },
              child: const Text('Show Detail')),
          AnimatedOpacity(
            opacity: _opacity,
            duration: const Duration(seconds: 2),
            child: const Column(
              children: [
                Text('Type: Owl'),
                Text('Age: 39'),
                Text('Employment: None'),
              ],
            ),
          )
        ],
      ),
    );
  }
}
