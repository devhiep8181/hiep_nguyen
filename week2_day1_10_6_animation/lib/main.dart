import 'package:flutter/material.dart';
import 'package:week2_day1_10_6_animation/implicit_animation/custom_shap_animation.dart';
import 'package:week2_day1_10_6_animation/implicit_animation/fade_in_animation.dart';
import 'package:week2_day1_10_6_animation/implicit_animation/shap_animation.dart';

void main(List<String> args) {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    // return const FadeInAnimation();
    //return const ShapAnimation();
    return const CustomShapAnimation();
  }
}
