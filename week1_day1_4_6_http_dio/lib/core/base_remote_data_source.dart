import 'package:week1_day1_4_6_http_dio/utils/api_constant.dart';

abstract class BaseRemoteDataSoure {
  Future<dynamic> performGetRequest({String serverAddress = api, required String url});
  Future<dynamic> performPostRequest(
      {String serverAddress = api, required Map<String, dynamic> body, required String url});
  Future<dynamic> performPutRequest(
      {String serverAddress = api, required Map<String, dynamic> body, required String url});
  Future<dynamic> performDeletRequest(
      {String serverAddress = api,required String url});
}
