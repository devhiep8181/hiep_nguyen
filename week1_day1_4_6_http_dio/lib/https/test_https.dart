import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:week1_day1_4_6_http_dio/entites/product_entity.dart';
import 'package:week1_day1_4_6_http_dio/https/base_https.dart';
import 'package:week1_day1_4_6_http_dio/utils/api_constant.dart';

class TestHttps extends StatelessWidget {
  const TestHttps({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Test Https'),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
                onPressed: () async {
                  final data = await RemoteDataSourceByHttps()
                      .performGetRequest(url: endpointObject);
                  final dataList = jsonDecode(data) as List<dynamic>;
                  final resultData = dataList
                      .map((data) => ProductEntity.fromMap(data))
                      .toList();
                  resultData.forEach((element) {
                    print('element: $element');
                  });
                },
                child: const Text('Get data')),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () {
                  RemoteDataSourceByHttps().performPostRequest(body: {
                    "name": "King of Men",
                    "data": {
                      "year": 2024,
                      "price": 1849.99,
                      "CPU model": "Intel Core i9",
                      "Hard disk size": "1 TB"
                    }
                  }, url: endpointObject);
                },
                child: const Text('Post data')),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () {
                  RemoteDataSourceByHttps().performPutRequest(body: {
                    "name": "yeah",
                    "data": {
                      "year": 999999,
                      "price": 1849.99,
                      "CPU model": "Intel Core i9",
                      "Hard disk size": "1 TB"
                    }
                  }, url: endpointObjectById('5'));
                },
                child: const Text('Put data')),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () {
                  // RemoteDataSourceByHttps().performDeletRequest(url: )
                },
                child: const Text('Delete data')),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
