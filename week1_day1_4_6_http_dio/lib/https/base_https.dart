import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:week1_day1_4_6_http_dio/core/base_remote_data_source.dart';
import 'package:week1_day1_4_6_http_dio/utils/api_constant.dart';

class RemoteDataSourceByHttps implements BaseRemoteDataSoure {
  @override
  Future performDeletRequest(
      {String serverAddress = api, required String url}) async {
    try {
      final http.Response res = await http.delete(Uri.parse('$api$url'));
      if (res.statusCode == 200) {
        log('delete data: ${res.body}');
      } else {
        log('error: ${res.statusCode} --- ${res.request}');
      }
    } catch (e) {
      log('error');
    }
  }

  @override
  Future performGetRequest(
      {String serverAddress = api, required String url}) async {
    try {
      final http.Response res = await http.get(Uri.parse('$api$url'));
      if (res.statusCode == 200) {
        return res.body;
      } else {
        log('error: ${res.statusCode} --- ${res.request}');
      }
    } catch (e) {
      log('error');
    }
  }

  @override
  Future performPostRequest(
      {String serverAddress = api,
      required Map<String, dynamic> body,
      required String url}) async {
    try {
      final http.Response res = await http.post(Uri.parse('$api$url'),
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode(body));
      if (res.statusCode == 200) {
        log('add: ${res.body}');
      } else {
        log('error: ${res.statusCode} --- ${res.request}');
      }
    } catch (e) {
      log('error');
    }
  }

  @override
  Future performPutRequest(
      {String serverAddress = api,
      required Map<String, dynamic> body,
      required String url}) async {
    try {
      final http.Response res = await http.put(Uri.parse('$api$url'),
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode(body));
      if (res.statusCode == 200) {
        log('update succes: ${res.body}');
      } else {
        log('error: ${res.statusCode} --- ${res.request}');
      }
    } catch (e) {
      log('error');
    }
  }
}
