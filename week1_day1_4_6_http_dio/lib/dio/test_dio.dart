import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class TestDio extends StatelessWidget {
  const TestDio({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              getHttp();
            },
            child: const Text('Call Api')),
      ),
    );
  }
}

final dio = Dio();

void getHttp() async {
  final response = await dio.get('https://dart.dev');
  print(response);
}
