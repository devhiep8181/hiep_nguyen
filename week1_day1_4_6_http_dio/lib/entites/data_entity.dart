import 'dart:convert';

import 'package:equatable/equatable.dart';

class DataEntity extends Equatable {
  final String color;
  final String capacity;
  const DataEntity({
    required this.color,
    required this.capacity,
  });

  @override
  List<Object?> get props => [color, capacity];

  Map<String, dynamic> toMap() {
    return {
      'color': color,
      'capacity': capacity,
    };
  }

  factory DataEntity.fromMap(Map<String, dynamic>? map) {
    return DataEntity(color: map?['color'] ?? '', capacity: map?['capacity'] ?? '');
  }

  String toJson() => json.encode(toMap());

  factory DataEntity.fromJson(String source) =>
      DataEntity.fromMap(json.decode(source));
}
