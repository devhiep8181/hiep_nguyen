import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:week1_day1_4_6_http_dio/entites/data_entity.dart';

class ProductEntity extends Equatable {
  final String id;
  final String name;
  final DataEntity? dataEntity;

  const ProductEntity({
    required this.id,
    required this.name,
    required this.dataEntity,
  });

  @override
  List<Object?> get props => [id, name];

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'data': dataEntity?.toMap(),
    };
  }

  factory ProductEntity.fromMap(Map<String, dynamic> map) {
    return ProductEntity(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      dataEntity:  DataEntity.fromMap(map['data']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductEntity.fromJson(String source) =>
      ProductEntity.fromMap(json.decode(source));
}
