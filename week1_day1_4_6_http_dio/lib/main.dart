import 'package:flutter/material.dart';
import 'package:week1_day1_4_6_http_dio/dio/test_dio.dart';

import 'https/test_https.dart';

void main(List<String> args) {
  runApp(const AppHttps());
}

class AppHttps extends StatelessWidget {
  const AppHttps({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: true,
      home: TestDio(),
    );
  }
}



