import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/repositories/weather_repository.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/usecases/get_current_weather._usecase.dart';
import 'package:mocktail/mocktail.dart';

class MockWeatherRepository extends Mock implements WeatherRepository {}

void main() {
  late GetCurrentWeatherUseCase getCurrentWeatherUseCase;
  late MockWeatherRepository mockWeatherRepository;

  setUp(() {
    mockWeatherRepository = MockWeatherRepository();
    getCurrentWeatherUseCase =
        GetCurrentWeatherUseCase(weatherRepository: mockWeatherRepository);
  });

//khoi tao ket qua mong muon
  const testWeatherDetail = WeatherEntity(
    cityName: 'New York',
    main: 'Clouds',
    description: 'few clouds',
    iconCode: '02d',
    temperature: 302.28,
    pressure: 1009,
    humidity: 70,
  );

  const testCityName = 'New York';

  test('should get current weather detail from the repository', () async {
    //stubbing
    when(() => mockWeatherRepository.getCurrentWeather(cityName: testCityName))
        .thenAnswer((invocation) async => const Right(testWeatherDetail));

    //act
    final result = await getCurrentWeatherUseCase.call(testCityName);

    //expect
    expect(result, const Right(testWeatherDetail));
  });
}
