import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/failure.dart';

import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/usecases/get_current_weather._usecase.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/presentation/bloc/weather/weather_bloc.dart';

class MockGetWeatherUseCase extends Mock implements GetCurrentWeatherUseCase {}

void main() {
  late MockGetWeatherUseCase mockGetWeatherUseCase;
  late WeatherBloc weatherBloc;

  setUp(() {
    mockGetWeatherUseCase = MockGetWeatherUseCase();
    weatherBloc = WeatherBloc(getCurrentWeatherUseCase: mockGetWeatherUseCase);
  });

  const testWeather = WeatherEntity(
    cityName: 'New York',
    main: 'Clouds',
    description: 'few clouds',
    iconCode: '02d',
    temperature: 302.28,
    pressure: 1009,
    humidity: 70,
  );

  const testCityName = 'New York';

  test('trạng thái khởi tạo rỗng', () {
    expect(weatherBloc.state, WeatherEmpty());
  });

  blocTest<WeatherBloc, WeatherState>(
    'should emit [WeatherLoading, WeatherLoaded] when data is gotten successfully',
    build: () {
      when(() => weatherBloc.getCurrentWeatherUseCase.call(testCityName))
          .thenAnswer((_) async => const Right(testWeather));
      return weatherBloc;
    },
    act: (bloc) => bloc.add(const CityChangedEvent(cityName: testCityName)),
    wait: const Duration(milliseconds: 500),
    expect: () =>
        [WeatherLoading(), const WeatherLoaded(weatherEntity: testWeather)],
  );

  blocTest(
      'should emit [WeatherLoading, WeatherLoadFailure] when get data is unsuccessful',
      build: () {
    when(() => weatherBloc.getCurrentWeatherUseCase.call(testCityName))
        .thenAnswer((invocation) async =>
            const Left(ServerFailure(message: 'Server failure')));
    return weatherBloc;
  },
  act: (bloc) => bloc.add(const CityChangedEvent(cityName: testCityName)),
  wait: const Duration(milliseconds: 500),
  expect: () => [WeatherLoading(), const WeatherLoadFailure(message: 'Server failure' )]
  );
}
