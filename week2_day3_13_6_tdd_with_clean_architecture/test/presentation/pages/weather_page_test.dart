// import 'package:bloc_test/bloc_test.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:mocktail/mocktail.dart';
// import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
// import 'package:week2_day3_13_6_tdd_with_clean_architecture/presentation/bloc/weather/weather_bloc.dart';
// import 'package:week2_day3_13_6_tdd_with_clean_architecture/presentation/pages/weather_page.dart';

// class MockWeatherBloc extends MockBloc<WeatherEvent, WeatherState>
//     implements WeatherBloc {}

// void main() {
//   late MockWeatherBloc mockWeatherBloc;

//   setUp(() {
//     mockWeatherBloc = MockWeatherBloc();
//   });

//   Widget _makeTestableWidget(Widget body) {
//     return BlocProvider(
//       create: (context) => mockWeatherBloc,
//       child: MaterialApp(
//         home: body,
//       ),
//     );
//   }

//   const testWeather = WeatherEntity(
//     cityName: 'New York',
//     main: 'Clouds',
//     description: 'few clouds',
//     iconCode: '02d',
//     temperature: 302.28,
//     pressure: 1009,
//     humidity: 70,
//   );

//   testWidgets('text field kích hoạt trạng thái thay đổi từ empty -> loading',
//       (widgetTester) async {
//     //mock
//     when(() => mockWeatherBloc.state).thenReturn(WeatherEmpty());
//     //act
//     await widgetTester.pumpWidget(_makeTestableWidget(const WeatherPage()));
//     var textFiled = find.byType(TextField);
//     expect(textFiled, findsOneWidget);
//     await widgetTester.enterText(textFiled, 'New York');
//     await widgetTester.pump();
//     expect(find.text('New York'), findsOneWidget);
//   });
// }
