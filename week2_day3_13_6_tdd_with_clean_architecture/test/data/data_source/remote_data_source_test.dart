import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:http/http.dart' as http;
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/constants/constants.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/exception.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/data_source/weather_remote_data_source.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/models/weather_model.dart';

import '../../helpers/json_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  late MockHttpClient mockHttpClient;
  late WeatherRemoteDataSoureImpl weatherRemoteDataSoureImpl;

  setUp(() {
    mockHttpClient = MockHttpClient();
    weatherRemoteDataSoureImpl =
        WeatherRemoteDataSoureImpl(client: mockHttpClient);
  });

  const testCityName = 'New York';

  group('phương thức get weather', () {
    test('gọi dữ liệu phải thành công', () async {
      //mock
      when(() => mockHttpClient
              .get(Uri.parse(Urls.currentWeatherByName(testCityName))))
          .thenAnswer((invocation) async {
        return http.Response(
            readJson('helpers/dummy_data/dummy_weather_response.json'), 200);
      });

      //act
      final result = await weatherRemoteDataSoureImpl.getCurrentWeather(
          cityName: testCityName);

      //expect
      expect(result, isA<WeatherModel>());
    });

    test('gọi dữ liệu phải thất bại', () async {
      //mock => để thực hiện gọi get trả về 404 => kiểm tra trường hợp lỗi
      when(() => mockHttpClient
              .get(Uri.parse(Urls.currentWeatherByName(testCityName))))
          .thenAnswer((invocation) async => http.Response('Not found', 404));

      //act
      final result =
           weatherRemoteDataSoureImpl.getCurrentWeather(cityName: testCityName);

      expect(result, throwsA(isA<ServerException>()));
    });
  });
}
