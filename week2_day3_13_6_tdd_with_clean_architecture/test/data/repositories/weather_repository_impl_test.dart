import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/exception.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/failure.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/data_source/weather_remote_data_source.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/models/weather_model.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/repositories/weather_repository_impl.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/repositories/weather_repository.dart';

class MockWeatherRemoteDataSource extends Mock
    implements WeatherRemoteDataSoure {}

class MockFailure extends Mock implements Failure {}

void main() {
  late MockWeatherRemoteDataSource mockWeatherRemoteDataSource;
  late WeatherRepositoryImpl weatherRepositoryImpl;
  late MockFailure mockFailure;

  setUp(() {
    mockWeatherRemoteDataSource = MockWeatherRemoteDataSource();
    weatherRepositoryImpl = WeatherRepositoryImpl(
        weatherRemoteDataSoure: mockWeatherRemoteDataSource);
    mockFailure = MockFailure();
  });

  const testWeatherModel = WeatherModel(
    cityName: 'New York',
    main: 'Clouds',
    description: 'few clouds',
    iconCode: '02d',
    temperature: 302.28,
    pressure: 1009,
    humidity: 70,
  );

  const testWeatherEntity = WeatherEntity(
    cityName: 'New York',
    main: 'Clouds',
    description: 'few clouds',
    iconCode: '02d',
    temperature: 302.28,
    pressure: 1009,
    humidity: 70,
  );

  const testCityName = 'New York';

  group('kiểm thử repository weather', () {
    test('phải trả về weatherEnity', () async {
      //mock, stubbing
      when(() => mockWeatherRemoteDataSource.getCurrentWeather(
              cityName: testCityName))
          .thenAnswer((invocation) async => testWeatherModel);

      //act
      final result =
          await weatherRepositoryImpl.getCurrentWeather(cityName: testCityName);

      //kiểm tra xem có đúng là kết quả trả về testWeatherEntity không
      const expectedResult = Right<Failure, WeatherEntity>(testWeatherEntity);

      expect(result, equals(expectedResult));
    });

    test('phải trả về lỗi ServerFailure', () async {
      //stubbing
      when(() => mockWeatherRemoteDataSource.getCurrentWeather(
          cityName: testCityName)).thenThrow(ServerException());

      //act
      final result =
          await weatherRepositoryImpl.getCurrentWeather(cityName: testCityName);

      //except
      const expectedResult = Left<Failure, WeatherEntity>(
          ServerFailure(message: 'An error has occurred'));

      expect(result, equals(expectedResult));
    });

    test('phải trả lỗi Connection Failure', () async {
      //mock
      when(() => mockWeatherRemoteDataSource.getCurrentWeather(
          cityName: testCityName)).thenThrow(const SocketException('Failed to connect to the network'));

      //act
      final result =
          await weatherRepositoryImpl.getCurrentWeather(cityName: testCityName);

      const exceptedResult = Left<Failure, WeatherEntity>(
          ConnectionFailure(message: 'Failed to connect to the network'));

      //excepted
      expect(result, equals(exceptedResult));
    });
  });
}
