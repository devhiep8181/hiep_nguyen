import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/models/weather_model.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';

import '../../helpers/json_reader.dart';

void main() {
  const testWeatherModel = WeatherModel(
    cityName: 'New York',
    main: 'Clear',
    description: 'clear sky',
    iconCode: '01n',
    temperature: 292.87,
    pressure: 1012,
    humidity: 70,
  );

  test('phải là kiểu con của weather entity', () {
    expect(testWeatherModel, isA<WeatherEntity>());
  });

  test('dữ liệu phải trả về một đối tượng từ json', () {
    //mock
    final Map<String, dynamic> jsonMap =
        json.decode(readJson('helpers/dummy_data/dummy_weather_response.json'));

    //act
    final result = WeatherModel.fromMap(jsonMap);

    //expect
    expect(result, equals(testWeatherModel));
  });

  test('dữ liệu phải chuyển được từ object sang json', () {
    //mock
    final expactedJsonMap = {
      'weather': [
        {
          'main': 'Clear',
          'description': 'clear sky',
          'icon': '01n',
        }
      ],
      'main': {
        'temp': 292.87,
        'pressure': 1012,
        'humidity': 70,
      },
      'name': 'New York',
    };

    //act
    final result = testWeatherModel.toMap();

    //excepted
    expect(result, equals(expactedJsonMap));
  });
}
