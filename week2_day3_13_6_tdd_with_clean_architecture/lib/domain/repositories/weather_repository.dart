import 'package:dartz/dartz.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/failure.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';

abstract class WeatherRepository {
  Future<Either<Failure, WeatherEntity>> getCurrentWeather({required String cityName});
}
