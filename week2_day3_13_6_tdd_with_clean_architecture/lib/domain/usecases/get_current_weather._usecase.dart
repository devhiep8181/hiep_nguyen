import 'package:dartz/dartz.dart';

import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/failure.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/usecase/usecase.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/repositories/weather_repository.dart';

class GetCurrentWeatherUseCase implements UseCase<WeatherEntity, String> {
  final WeatherRepository weatherRepository;
  GetCurrentWeatherUseCase({
    required this.weatherRepository,
  });

  @override
  Future<Either<Failure, WeatherEntity>> call(String params) async {
    return weatherRepository.getCurrentWeather(cityName: params);
  }
}
