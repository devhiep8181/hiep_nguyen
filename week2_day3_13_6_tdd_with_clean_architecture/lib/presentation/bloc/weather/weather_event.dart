part of 'weather_bloc.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent();

  @override
  List<Object> get props => [];
}

class CityChangedEvent extends WeatherEvent {
  final String cityName;
  const CityChangedEvent({
    required this.cityName,
  });

  @override
  List<Object> get props => [cityName];
  
}
