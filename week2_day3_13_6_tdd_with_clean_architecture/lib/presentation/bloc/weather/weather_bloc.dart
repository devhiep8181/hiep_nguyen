import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/usecases/get_current_weather._usecase.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc({required this.getCurrentWeatherUseCase})
      : super(WeatherEmpty()) {
    on<CityChangedEvent>(_onCityChangedEvent,
        transformer: debounce(const Duration(milliseconds: 500)));
  }

  final GetCurrentWeatherUseCase getCurrentWeatherUseCase;

  FutureOr<void> _onCityChangedEvent(
      CityChangedEvent event, Emitter<WeatherState> emit) async {
    emit(WeatherLoading());

    final result = await getCurrentWeatherUseCase.call(event.cityName);

    result.fold((failure) => emit(WeatherLoadFailure(message: failure.message)),
        (success) => emit(WeatherLoaded(weatherEntity: success)));
  }

  EventTransformer<T> debounce<T>(Duration duration) {
    return (events, mapper) => events.debounceTime(duration).flatMap(mapper);
  }
}
