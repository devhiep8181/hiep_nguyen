part of 'weather_bloc.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

class WeatherEmpty extends WeatherState {}

class WeatherLoading extends WeatherState {}

class WeatherLoaded extends WeatherState {
  final WeatherEntity weatherEntity;
  const WeatherLoaded({
    required this.weatherEntity,
  });
  @override
  List<Object> get props => [weatherEntity];
}

class WeatherLoadFailure extends WeatherState {
  final String message;
  const WeatherLoadFailure({
    required this.message,
  });

  @override
  List<Object> get props => [message];
}
