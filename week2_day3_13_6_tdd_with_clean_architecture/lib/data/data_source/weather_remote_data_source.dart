import 'package:http/http.dart' as http;
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/constants/constants.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/exception.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/failure.dart';

import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/models/weather_model.dart';

abstract class WeatherRemoteDataSoure {
  Future<WeatherModel> getCurrentWeather({required String cityName});
}

class WeatherRemoteDataSoureImpl implements WeatherRemoteDataSoure {
  final http.Client client;

  WeatherRemoteDataSoureImpl({
    required this.client,
  });

  @override
  Future<WeatherModel> getCurrentWeather({required String cityName}) async {
    final response =
        await client.get(Uri.parse(Urls.currentWeatherByName(cityName)));

    if (response.statusCode == 200) {
      return WeatherModel.fromJson(response.body);
    } else {
      throw ServerException();
    }
  }
}
