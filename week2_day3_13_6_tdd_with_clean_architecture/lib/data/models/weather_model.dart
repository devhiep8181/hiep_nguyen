import 'dart:convert';

import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';

class WeatherModel extends WeatherEntity {
  const WeatherModel(
      {required super.cityName,
      required super.main,
      required super.description,
      required super.iconCode,
      required super.temperature,
      required super.pressure,
      required super.humidity});

  Map<String, dynamic> toMap() {
    return {
      'weather': [
        {
          'main': main,
          'description': description,
          'icon': iconCode,
        },
      ],
      'main': {
        'temp': temperature,
        'pressure': pressure,
        'humidity': humidity,
      },
      'name': cityName,
    };
  }

  factory WeatherModel.fromMap(Map<String, dynamic> map) {
    return WeatherModel(
      cityName: map['name'] ?? '',
      main: map['weather'][0]['main'] ?? '',
      description: map['weather'][0]['description'] ?? '',
      iconCode: map['weather'][0]['icon'] ?? '',
      temperature: map['main']['temp']?.toDouble() ?? 0.0,
      pressure: map['main']['pressure']?.toInt() ?? 0,
      humidity: map['main']['humidity']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory WeatherModel.fromJson(String source) =>
      WeatherModel.fromMap(json.decode(source));

  WeatherEntity toEntity() => WeatherEntity(
      cityName: cityName,
      main: main,
      description: description,
      iconCode: iconCode,
      temperature: temperature,
      pressure: pressure,
      humidity: humidity);
}
