import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/exception.dart';

import 'package:week2_day3_13_6_tdd_with_clean_architecture/core/error/failure.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/data_source/weather_remote_data_source.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/entities/weather.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/repositories/weather_repository.dart';

class WeatherRepositoryImpl implements WeatherRepository {
  final WeatherRemoteDataSoure weatherRemoteDataSoure;
  WeatherRepositoryImpl({
    required this.weatherRemoteDataSoure,
  });

  @override
  Future<Either<Failure, WeatherEntity>> getCurrentWeather(
      {required String cityName}) async {
    try {
      final result =
          await weatherRemoteDataSoure.getCurrentWeather(cityName: cityName);
      return Right(result.toEntity());
    } on ServerException {
      return const Left(ServerFailure(message: 'An error has occurred'));
    } on SocketException {
      return const Left(
          ConnectionFailure(message: 'Failed to connect to the network'));
    }
  }
}
