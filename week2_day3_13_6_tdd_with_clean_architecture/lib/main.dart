import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/app/dependecy_injecttion.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/presentation/bloc/weather/weather_bloc.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/presentation/pages/weather_page.dart';

void main(List<String> args) {
  runApp(repositoryProviders(child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          WeatherBloc(getCurrentWeatherUseCase: context.read()),
      child: const MaterialApp(
        home: WeatherPage()
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('hihi'),
      ),
    );
  }
}
