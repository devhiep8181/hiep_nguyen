import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/data_source/weather_remote_data_source.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/data/repositories/weather_repository_impl.dart';
import 'package:week2_day3_13_6_tdd_with_clean_architecture/domain/usecases/get_current_weather._usecase.dart';
import 'package:http/http.dart' as http;

MultiRepositoryProvider repositoryProviders({required Widget child}) {
  return MultiRepositoryProvider(providers: [
    //!Weather
    RepositoryProvider(
        create: (context) => GetCurrentWeatherUseCase(
            weatherRepository: WeatherRepositoryImpl(
                weatherRemoteDataSoure:
                    WeatherRemoteDataSoureImpl(client: http.Client()))))
  ], child: child);
}
